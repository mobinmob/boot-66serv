title: The 66 Suite: boot@-66serv
author: Eric Vidal <eric@obarun.org>

# boot@ service

The boot@ instantiated service is a portable set of services to boot a machine in conjunction with the 66 API.

## How to enable

Like any other instantiated service to enable it you need to specify the target of the service. The target can be a random name.

This is a specific service that handles the boot sequence of a machine, it is usually enabled on a tree build especially for it and contains only this service.

For example:

```
# 66-tree -n boot
# 66-enable -t boot boot@system
```
The name of the tree need to match the name set at your `TREE=` *key=value* pair defined in the `%%skel_directory%%/init.conf` file.

Do not mark this tree enabled with the `66-tree -E` option. The [66-init](https://web.obarun.org/software/66/66-tree.html) will take care of this special tree and start it automatically.

Refer to the [66-tree](https://web.obarun.org/software/66/66-tree.html) and [66-enable](https://web.obarun.org/software/66/66-tree.html) for further information on these tools.

## How to configure it

This service is declared as a `module` service and comes with its own configuration file.
The configuration file can be seen and edited easily using the [66-env](https://web.obarun.org/software/66/66-env.html) tool:

```
# 66-env boot@system
```

When you are done editing this file, you need to reenable the service to have changes applied by using the -F option:

```
# 66-enable -t boot -F boot@system
```

## Configuration file key=value pair explanation

Two different kinds of variables are used:

- boolean: accepts `yes` or `no` as the only valid values, and nothing else.

- string: depends on the variable and is defined in the following documentation.

---

- **HOSTNAME**:
	- type: string.
	- valid value: anything you want.
	- description: informs the kernel of the hostname to use and sets the /etc/hostname file with the value declared.

- **TZ** (TimeZone):
	- type: string.
	- valid value: any valid name found at /usr/share/zoneinfo.
	- description: creates a symlink at `/etc/localtime` pointing to the `/usr/share/zoneinfo` directory definition. Usually the value contains a slash like Pacific/Noumea, America/New_york,...

- **KEYMAP**:
	- type: string.
	- valid value: any valid name found at `/usr/share/kbd/keymaps` directory.
	- description: uses the `loadkeys` program to set the keymap for the console to use. Usually a simple string like fr, us,...
	
- **FONT**:
	- type: string.
	- valid value: any valid name found at `/usr/share/kbd/consolefonts`.
	- description: uses the `setfont` program to set the font for the console to use. Usually a simple string like lat9w-16, alt-8x14,...

- **FONT_MAP**:
	- type: string.
	- valid value: any valid name found at `/usr/share/kbd/consoletrans`.
	- description: uses the `setfont` program to indicate the mapping to use. Usually a simple string like 8859-1, 8859-2,...

- **FONT_UNIMAP**:
	- type: string.
	- valid value: any valid name found at `/usr/share/kbd/unimaps`.
	- description: uses the `setfont` program to indicate the unicode mapping table to use. Usually no mapping table is needed, and a Unicode mapping table is already contained in the font.

- **FSTAB**:
	- type: boolean.
	- description: uses the `mount -a` command to mount each filesystem set at `/etc/fstab`.
	
- **SWAP**:
	- type: boolean.
	- description: uses the `swapon -a` command to activate your swap partitions/files.
	
- **LVM**:
	- type: boolean.
	- description: uses the `lvmetad` and `vgchange` program to activate LVM partitions. If these programs is not installed on your system, the boot process **fail** even if the enable process finish successfully.

- **DMRAID**:
	- type: boolean.
	- description: uses the `dmraid` program to activate RAID partitions. If the program is not installed on your system, the boot process **fail** even if the enable process finish successfully.

- **BTRFS**:
	- type: boolean.
	- description: uses the `btrfs` program to activate BTRFS partitions. If the program is not installed on your system, the boot process **fail** even if the enable process finish successfully.
	
- **ZFS**:
	- type: boolean.
	- description: uses the `zfs` program to mount ZFS partitions. If the program is not installed on your system, the boot process **fail** even if the enable process finish successfully.

- **ZFS_IMPORT**:
	- type: string.
	- valid value: scan or zpoolcache.
	- description: select the import method to use to detect the ZFS devices. This variable works in conjunction with the `ZFS` variable. In case the `ZFS` variable is set to no or commented, the `ZFS_IMPORT` variable has no effect.

- **SETUPCONSOLE**:
	- type: boolean.
	- description: set it to no if you don't want to configure the console. In this case the `KEYMAP,FONT`, `FONT_MAP`, `FONT_UNIMAP` variable has no effect.

- **HARDWARECLOCK**:
	- type: string.
	- valid value: UTC or locatime.
	- description: Restore the system clock to the given timezone set at `TZ` variable and set the hardware clock with timescale given (A.K.A. UTC or localtime).

- **UDEV**:
	- type: boolean.
	- description: use the udev program to detect the devices e.g. network card, external hard drive, .... If it's set to no the `SETUPCONSOLE`, `KEYMAP`, `FONT`, `FONT_MAP`, `FONT_UNIMAP`, `CRYPTTAB`, `DMRAID`, `BTRFS`, `LVM` variable has no effect.

- **SYSCTL**:
	- type: boolean.
	- description: use the sysctl program to change the kernel parameters at runtime. The `/etc/sysctl.conf` ***must*** exist on your system and be correctly written.

- **FORCECHCK**:
	- type: boolean.
	- description: force the check of all mounted file systems.

- **LOCAL**:
	- type: boolean.
	- description: use the `%%skel_directory%%/rc.local` script. This script is launched at the end of the boot procedure. The file ***must*** exist on your system and be set as executable, with correct definition of the shebang at the start of the script.
	
- **CONTAINER**:
	- type: boolean.
	- description: convenient variable used to boot inside a container. Some services will not work or be unnecessary when you boot inside a container. This variable provides you a safe default for this purpose. If set to yes the `HARDWARECLOCK`, `SETUPCONSOLE`, `KEYMAP`, `FONT`, `FONT_MAP`, `FONT_UNIMAP`, `CRYPTTAB`, `SWAP`, `LVM`, `DMRAID`, `BTRFS`, `ZFS`, `UDEV`, `SYSCTL`, `FORCECHCK`, `CGROUPS`, `MODULE_SYSTEM`, `RANDOMSEED`, `MNT_NETFS` variables have no effect.

- **TMPFILES**:
	- type: boolean.
	- description: read, parse and apply a systemd-style tmpfiles.d files usually found at `/usr/lib/tmpfiles.d` directory to handle volatile and temporary files.

- **MODULE_KERNEL**:
	- type: boolean.
	- description: load the kernel modules returned by the `kmod static-nodes` command.

- **MODULE_SYSTEM**:
	- type: boolean.
	- description: read, parse and load the modules found at `/etc/modules-load.d`, `/run/modules-load.d`, `/usr/lib/modules-load.d`.

- **RANDOMSEED**:
	- type: boolean.
	- description: generates random numbers and saves them in `/var/lib/random-seed` if it doesn't already exist.

- **CRYPTTAB**:
	- type: boolean.
	- description: use the file `/etc/crypttab` to decrypt an encrypted device. The file `/etc/crypttab` ***must*** exist on your system.

- **FIREWALL**:
	- type: string.
	- valid value: iptables,ip6tables,nftables,ebtables,arptables.
	- description: use the given program to set a firewall applying the corresponding configuration files. In case of iptables and ip6tables the `/etc/<program>/<program>.rules` file ***must*** exist on your system. In other cases the `/etc/<program>.conf` ***must*** exist on your system.

- **CGROUPS**:
	- type: boolean.
	- description: read and parse the `/proc/cgroups` file and mount the corresponding `/sys/fs/cgroup/<groups>` directory. The `/sys/fs/cgroup/unified` is also mounted.

- **MNT_PROC**:
	- type: boolean.
	- description: check and mount the `/proc` directory if it is not mounted yet.

- **MNT_SYS**:
	- type: boolean.
	- description: check and mount the `/sys` directory if it is not mounted yet.

- **MNT_DEV**:
	- type: boolean.
	- description: check and mount the `/dev` directory if it is not mounted yet.

- **MNT_RUN**:
	- type: boolean.
	- description: check and mount the `/run` directory if it is not mounted yet.

- **MNT_TMP**:
	- type: boolean.
	- description: check and mount the `/tmp` directory if it is not mounted yet.

- **MNT_PTS**:
	- type: boolean.
	- description: check and mount the `/dev/pts` directory if it is not mounted yet.

- **MNT_SHM**:
	- type: boolean.
	- description: check and mount the `/dev/shm` directory if it is not mounted yet.

- **MNT_NETFS**:
	- type: boolean.
	- description: mount all file systems with the command `mount -a -t nosysfs,nonfs,nonfs4,nosmbfs,nocifs -O no_netdev`.

- **POPULATE_SYS**:
	- type: boolean
	- decription: mount the `/sys/firmware/efi/efivars`, `/sys/fs/fuse/connections`, `/sys/kernel/config`, `/sys/kernel/debug` and `/sys/kernel/security` directories.

- **POPULATE_DEV**:
	- type: boolean
	- description: mount the `/dev/hugepages` and `/dev/mqueue` directories.

- **POPULATE_RUN**:
	- type: boolean
	- description: mount the `/run/lvm`, `/run/user` and `/run/lock` directories.

- **POPULATE_TMP**:
	- type: boolean
	- description: create the `/tmp/.X11-unix` and `/tmp/.ICE-unix` directories.
