#!/bin/sh

man1='boot@'

if [ ! -d doc/man/man1 ]; then
    mkdir -p -m 0755 doc/man/man1 || exit 1
fi


for i in ${man1}; do
    lowdown -s -Tman doc/"${i}".md -o doc/man/man1/"${i}".1 || exit 1
    var=$(sed -n -e '/^.TH/p' < doc/man/man1/"${i}".1)
    var=$(printf '%s' "$var" | tr '7' '1')
    var="${var} \"\" \"General Commands Manual\""
    sed -i "s!^.TH.*!${var}!" doc/man/man1/"${i}".1 || exit 1
done

exit 0
